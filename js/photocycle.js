/*
* INFO/CS 1300
* Fall 2016
*
*
* A simple JavaScript file to swap images
*
*/

/* establish global variables */
/*global window, document*/

// variables
var pix = ["pic1.png", "pic2.png", "pic3.png", "pic4.png"],
imgnum = 1;

function next() {
    "use strict";
    if (imgnum == pix.length-1) {
        imgnum = 0;
    } else {
        imgnum++;
    }
    var main_image = document.getElementById("pic2")
    main_image.src = "images/" + pix[imgnum]; 
}

function prev() {
    "use strict";
    if (imgnum == 0) {
        imgnum = pix.length-1;
    } else {
        imgnum--;
    }
    var main_image = document.getElementById("pic2")
    main_image.src = "images/" + pix[imgnum]; 
}

function pic_change() {
    "use strict"
    if (imgnum == pix.length-1) {
        imgnum = 0;
    } else {
        imgnum++;
    }
    var main_image = document.getElementById("pic2")
    main_image.src = "images/" + pix[imgnum]; 
}

function pic_cycle() {
    "use strict";
    setInterval(pic_change, 5000);
}

// function call
pic_cycle();