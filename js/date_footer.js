/*
* INFO 1300
* Fall 2016
* Danielle Charpeniter
*

*/

// include window onload so date is inserted on page laod
window.onload = function date_and_season() {
    "use strict";
    // define variables
    var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        // get pieces of date
        full_date = new Date(),
        month = monthNames[full_date.getMonth()],
        // month = "October",
        today = full_date.getDate(),
        year = full_date.getFullYear(),
        // set information up for insertion into DOM tree
        new_span = document.createElement('span'),
        new_text = document.createTextNode(today + ' ' + month + ' ' + year),
        position = document.getElementsByTagName('footer')[0];
    // add date to footer
    new_span.appendChild(new_text);
    position.appendChild(new_span);
};


